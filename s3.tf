resource "aws_s3_bucket" "bucket_curso_devops" {
    bucket = "bucket-curso-devops-udea"
    acl = "public-read"
    tags = {
       Name = "Terraform bucket test"
    }
}
