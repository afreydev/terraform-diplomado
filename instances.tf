resource "aws_instance" "web_devops" {
  ami            = "${var.ami}"
  instance_type  = "${var.aws_instance_type}"
  security_groups = ["${aws_security_group.web.name}", "${aws_security_group.ssh.name}"]
  key_name = "${aws_key_pair.admin_key.key_name}"
  associate_public_ip_address = true
  count   =   1
  tags           = {
     Name = "Instancias lanzadas desde Terraform"
  }
  #provisioner "remote-exec" {
    # The connection will use the local SSH agent for authentication
  #  inline = ["echo Successfully connected"]

   # connection {
   #   user        = "ec2-user"
   #   type        = "ssh"
   #   private_key = "${file("${var.aws_ssh_admin_key_file}")}"
   # }
  #}

   # provisioner "local-exec" {
   #   command = "ansible-playbook --key-file=./${var.aws_ssh_admin_key_file} -i ${self.public_ip}, ./ansible/setup-apache.yml -vvv"
   # }

}
