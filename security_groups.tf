resource "aws_security_group" "web" {
  name          = "web"
  description   = "web"
  
  ingress {
    from_port    = 80
    to_port      = 80
    protocol     = "tcp"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "web_diplomado_devops"
  }
}

resource "aws_security_group" "ssh" {
  name          = "ssh"
  description   = "ssh"

  ingress {
    from_port    = 22
    to_port      = 22
    protocol     = "tcp"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh_diplomado_devops"
  }
}
