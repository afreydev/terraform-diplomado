variable "aws_access_key" {
  description = "AWS Access Key"
}

variable "aws_ssh_admin_key_file" {
  description = "Admin key file"
}

variable "aws_ssh_admin_key_name" {
  description = "Admin key name"
}

variable "aws_secret_key" {
  description = "AWS Secret Key"
}

variable "aws_region" {
  default = "us-east-1"
  description = "AWS Region"
}

variable "ami" {
  default = "ami-035b3c7efe6d061d5"
}

variable "aws_instance_type" {
  default = "t2.micro"
}
